#php
alias serve="php -S 127.0.0.1:7777"

#laravel
alias art="php artisan"
alias amodel="php artisan make:model -m"
alias acontroller="php artisan make:controller"
alias areqest="php artisan make:request"
alias aresource="php artisan make:resource"
alias aseeder="php artisan make:seeder"
alias acommand="php artisan make:command"
alias amigrate="php artisan migrate"
alias afresh="php artisan migrate:fresh"
alias aseed="php artisan db:seed"
alias afactory="php artisan make:factory"
alias amiddleware="php artisan make:middleware"
alias arule="php artisan make:rule"
alias aexception="php artisan make:exception"
alias aprovider="php artisan make:provider" 
alias atest="php artisan make:test"
alias sail="./vendor/bin/sail"

#quasar
alias qdev="qausar dev"

